import {MatButtonModule,MatCheckboxModule} from '@angular/material';
import {NgModule} from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
    imports:[MatToolbarModule,MatButtonModule,MatCheckboxModule,MatIconModule],
    exports:[MatToolbarModule,MatButtonModule,MatCheckboxModule,MatIconModule],
})
export class MaterialModule{}