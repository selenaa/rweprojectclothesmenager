import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { catchError } from 'rxjs/operators';
import { People } from '../models/people';
import {Room}from '../models/room';
import {Roommate}from '../models/roommates';
import {HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class DatabaseService {
  
  constructor(private http: HttpClient) { }

  getPeople():Observable<People[]>{
    return this.http.get<People[]>("http://localhost:3000/people")
    .pipe(catchError((error:any)=>Observable.throw(error.json())))
  }

  getRooms():Observable<Room[]>{
    return this.http.get<Room[]>("http://localhost:3000/rooms")
    .pipe(catchError((error:any)=>Observable.throw(error.json())))

  }

  getRoommates():Observable<Roommate[]>{
    return this.http.get<Roommate[]>("http://localhost:3000/roommate")
    .pipe(catchError((error:any)=>Observable.throw(error.json())))

  }

  


}