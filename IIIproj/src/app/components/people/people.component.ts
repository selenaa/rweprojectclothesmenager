import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import {People} from '../../models/people';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})

export class PeopleComponent implements OnInit {

  @Input() 
  public people: People
  @Input()
  public selected: boolean

  @Output()
  public selectedEvent: EventEmitter<People> = new EventEmitter()


  constructor() { }

  ngOnInit() {
  }

  selectPeople() {
    console.log(this.people)
    this.selectedEvent.emit(this.people)
  }
}
