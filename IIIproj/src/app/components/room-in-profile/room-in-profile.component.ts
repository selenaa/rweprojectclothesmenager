import{DeleteRoom}from '../../store/actions';
import{ EditRoom} from '../../store/actions';
import { Component, OnInit,Output, EventEmitter,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { People } from '../../models/people';
import {Store} from '@ngrx/store'
import { State } from '../../store';
import { Observable } from 'rxjs';
import {Room}from'../../models/room';
import {Roommate}from'../../models/roommates';
@Component({
  selector: 'app-room-in-profile',
  templateUrl: './room-in-profile.component.html',
  styleUrls: ['./room-in-profile.component.css']
})
export class RoomInProfileComponent implements OnInit {

  @Input() 
  public room: Room
  
  @Input()
  public selected: boolean

  @Output()
  public editEvent: EventEmitter<Room> = new EventEmitter()




  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private store$: Store<State>
  ) { }

  ngOnInit() {
  }

  deleteRoom() {
    console.log(this.room);
    this.store$.dispatch(new DeleteRoom(this.room));
  }
editRoom(){
  this.editEvent.emit(this.room)
  console.log("USAO U EDITROOM!")
  
}

}

