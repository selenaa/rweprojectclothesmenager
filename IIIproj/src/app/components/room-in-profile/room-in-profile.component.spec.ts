import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomInProfileComponent } from './room-in-profile.component';

describe('RoomInProfileComponent', () => {
  let component: RoomInProfileComponent;
  let fixture: ComponentFixture<RoomInProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomInProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomInProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
