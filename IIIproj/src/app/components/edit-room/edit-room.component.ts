
import{DeleteRoom} from '../../store/actions';
import { Component, OnInit,Output, EventEmitter,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { People } from '../../models/people';
import {Store} from '@ngrx/store'
import { State } from '../../store';
import { Observable } from 'rxjs';
import {Room}from'../../models/room';
import {Roommate}from'../../models/roommates';
import{UpdateValue}from '../../store/actions';

@Component({
  selector: 'app-edit-room',
  templateUrl: './edit-room.component.html',
  styleUrls: ['./edit-room.component.css']
})
export class EditRoomComponent implements OnInit {

  room$:Observable<Room[]>

  @Input()
  public room: Room


  constructor( private route: ActivatedRoute,
    private location: Location,
    private store$: Store<State>
   

  ) {
    this.room$=this.store$.select(state => state.roomEdit);
   }

  ngOnInit() {
  }
  updateValue(id:string,loc:string,places:string,picture:string,description:string){
  // this.store$.dispatch(new UpdateValue(id,"NoviSad","0","https://cdn1.imggmi.com/uploads/2018/6/16/6eb4e4ee5bb5696250c410fd1e621132-full.jpg","Promenjen opis"));
 this.store$.dispatch(new UpdateValue(id,loc,places,picture,description));

  }

}
