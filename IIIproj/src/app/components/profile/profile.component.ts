import { Component, OnInit,Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { People } from '../../models/people';
import {Store} from '@ngrx/store'
import { State } from '../../store';
import { Observable } from 'rxjs';
import {Room}from'../../models/room';
import {Roommate}from'../../models/roommates';
import {GetFromDatabaseRooms,GetFromDatabaseRoommates,EditRoom}from '../../store/actions';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  people$: Observable<People[]>
 // rooms$:Observable<Room[]>
 // roommates$:Observable<any>

 

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private store$: Store<State>
  ) {

    this.people$ = this.store$.select(state => state.people);
   // this.rooms$=this.store$.select(state => state.rooms);
   // this.roommates$=this.store$.select(state => state.roommates);
    console.log(this.people$);
    console.log(this.store$);

  }

  ngOnInit() {
    this.store$.dispatch(new GetFromDatabaseRooms());
   this.store$.dispatch(new GetFromDatabaseRoommates())
  }
  findRooms(){
    
  // this.store$.dispatch(new GetFromDatabaseRooms());
 console.log("Pozvao f-ju getFromDatabaseZaRooms!")
  }
  findRoommates(){
   // this.store$.dispatch(new GetFromDatabaseRoommates());
    console.log("Pozvao f-ju getFromDatabaseZaRoommates!")
  }
  onEditRoom(room:Room){
    console.log(room)
        this.store$.dispatch(new EditRoom(room))
        console.log("Usao u onEditRoom")!
      }
  

}
