import { Component, OnInit, Input,NgModule } from '@angular/core';
import {People}from '../../models/people';
import{Store}from '@ngrx/store'; 
import {State}from '../../store';
import {Observable}from'rxjs';
import {  GetFromDatabase } from '../../store/actions';
import {MatFormFieldModule} from '@angular/material';
@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

@Input()
public people$:Observable<People[]>
  constructor(private store$: Store<State>) {
    this.people$=this.store$.select(state=>state.people);
   }
 
  ngOnInit() {
  }
  sendUsAndPass(user:string,pass:string){
    
   // this.store$.dispatch(new GetFromDatabase("selenaaasi","2222"));
   this.store$.dispatch(new GetFromDatabase(user,pass));
   console.log(user,pass);
  }

}
