import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { People } from '../../models/people';
import {Store} from '@ngrx/store'
import { State } from '../../store';
import { Observable } from 'rxjs';
import {AddRoom}from '../../store/actions';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})


export class AddRoomComponent implements OnInit {

  constructor(   private route: ActivatedRoute,
    private location: Location,
    private store$: Store<State>) { }

  ngOnInit() {
  }
  addRoom(loc:string,spaces:string,picture:string,description:string){
    

   // this.store$.dispatch(new AddRoom("lokacija", 1,"https://cdn1.imggmi.com/uploads/2018/6/16/68609fa6520bc2b5800e624370fbd88b-full.jpg","opis"));
   this.store$.dispatch(new AddRoom(loc,spaces,picture,description));

  }

}
