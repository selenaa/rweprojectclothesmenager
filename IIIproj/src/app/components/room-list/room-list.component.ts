import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {Room} from '../../models/room';
import{People}from '../../models/people';
import {Observable}from 'rxjs';
import {Store} from '@ngrx/store'
import { State } from '../../store';
import {GetFromDatabaseRooms,EditRoom } from '../../store/actions';


@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  user$:Observable<People[]>
  rooms$:Observable<Room[]>
  
  constructor(   private route: ActivatedRoute,
    private location: Location,private store$: Store<State>) { 
      this.rooms$ = this.store$.select(state => state.rooms);
      this.user$=this.store$.select(state => state.people);
    
    }
    onEditRoom(room:Room){
  console.log(room)
      this.store$.dispatch(new EditRoom(room))
    }
  
    ngOnInit() {
    //  this.store$.dispatch(new GetFromDatabaseRooms());
    }

}
