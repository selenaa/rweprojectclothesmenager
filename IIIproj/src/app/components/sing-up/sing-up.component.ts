import { Component, OnInit } from '@angular/core';
import{Store}from '@ngrx/store'; 
import {State}from '../../store';
import{AddToDataBase}from '../../store/actions';
import{People}from '../../models/people';
import{Room}from '../../models/room';
@Component({
  selector: 'app-sing-up',
  templateUrl: './sing-up.component.html',
  styleUrls: ['./sing-up.component.css']
})
export class SingUpComponent implements OnInit {

  constructor(private store$: Store<State>) { }

  ngOnInit() {
  }
  addToDataBase(user:string,pass:string,name:string,surname:string,email:string,loc:string,gender:string,ages:string,picture:string){
   // let r= Room [];
    //let p=new People(10,"proba","proba","proba","proba","proba","p","p","p","https://cdn1.imggmi.com/uploads/2018/6/16/eb9ce2cd0a29e4955fb2a7621a403b0c-full.jpg")
   this.store$.dispatch(new AddToDataBase(user,pass,name,surname,email,loc,gender,ages,picture));
 
  }
}
