import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {Roommate} from '../../models/roommates';
import{People} from '../../models/people';
import {Observable}from 'rxjs';
import {Store} from '@ngrx/store'
import { State } from '../../store';
import {GetFromDatabaseRoommates } from '../../store/actions';

@Component({
  selector: 'app-roommate-list',
  templateUrl: './roommate-list.component.html',
  styleUrls: ['./roommate-list.component.css']
})
export class RoommateListComponent implements OnInit {

  user$:Observable<People[]>
  roommates$:Observable<any>
  constructor(   private route: ActivatedRoute,
    private location: Location,private store$: Store<State>) {
      this.roommates$ =this.store$.select(state => this.getRoommates(state));
      this.user$=this.store$.select(state => state.people);
     }

  ngOnInit() {
  }
  getRoommates(state: State) {
    if(state === undefined) {
      return [];
    } else {
      return Object.keys(state.roommates.entities).map((id) => state.roommates.entities[id]);
    }
  };

}
