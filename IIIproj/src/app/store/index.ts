import peopleReducer from './reducers/people.reducer';
import {People} from '../models/people';
import {Roommate}from '../models/roommates';
import{Action}from '@ngrx/store'; 
import {ActionReducerMap,createSelector} from '@ngrx/store';
import {Room}from '../models/room';
import roomsReducer from './reducers/rooms.reducer';
import roommatesReducer from './reducers/roommates.reducer';
import editRoomReducer from './reducers/editRoom.reducer';
import { EntityState } from "../../../node_modules/@ngrx/entity";
import * as fromRoommates from "./reducers/roommates.reducer";

export * from './effects';

export interface State {
    people: People[],
    rooms:Room[],
    roommates:fromRoommates.State,
    roomEdit:Room[]
 
}

export const rootReducer: ActionReducerMap<State> = {
    people: peopleReducer,
    rooms:roomsReducer,
    roommates:roommatesReducer,
    roomEdit:editRoomReducer
  
   
}

export const selectPeoples= (state: State) => state.people;
export const selectState = (state: State) => state;

export const selectRoommateEntities = createSelector(
    selectState,
    fromRoommates.selectAll
);

export const selectAllFolders = createSelector(
    selectRoommateEntities ,
    (entities) => {
        return Object.keys(entities).map(id => entities[id])
    }
);

