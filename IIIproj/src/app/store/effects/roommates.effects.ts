import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import {  } from "@";
import * as roommatesActions from '../actions';
import {of} from "rxjs";
import { switchMap,map } from 'rxjs/operators';
import * as databaseService from '../../services/database.service';

@Injectable()
export class RoommatesEffects {
  
 constructor(private actions$:Actions,private dbService:databaseService.DatabaseService){console.log("Ueffectu sam!")} 
   
 @Effect() //actions
 loadRoommates$=this.actions$.ofType(roommatesActions.GET_FROM_DATABASE_ROOMMATES)
  .pipe(
    switchMap(()=>{
    
        return this.dbService.getRoommates()
       
        .pipe(
            map(roommates=>new roommatesActions.DataArrivedRoommates(roommates),console.log("Usao u map za ROOMMATES!!!!!!!")),
          
        )
    }))



}