import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import {  } from "@";
import * as roomsActions from '../actions';
import {of} from "rxjs";
import { switchMap,map } from 'rxjs/operators';
import * as databaseService from '../../services/database.service';

@Injectable()
export class RoomsEffects {
  
 constructor(private actions$:Actions,private dbService:databaseService.DatabaseService){console.log("Ueffectu sam!")} 
   
 @Effect() //actions
 loadRooms$=this.actions$.ofType(roomsActions.GET_FROM_DATABASE_ROOMS)
  .pipe(
    switchMap(()=>{
    
        return this.dbService.getRooms()
       
        .pipe(
            map(rooms=>new roomsActions.DataArrivedRooms(rooms))
        )
    }))



}