import { PeopleEffects } from './people.effects';
import {RoomsEffects}from './rooms.effects';
import {RoommatesEffects}from './roommates.effects'
//import{AddUserEffects}from './add.user.effects';

export const effects:any[]=[PeopleEffects,RoomsEffects,RoommatesEffects];

export * from './people.effects';