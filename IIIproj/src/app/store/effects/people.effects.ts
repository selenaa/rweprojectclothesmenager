import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import {  } from "@";
import * as peopleActions from '../actions';
import {of} from "rxjs";
import { switchMap,map } from 'rxjs/operators';
import * as databaseService from '../../services/database.service';

@Injectable()
export class PeopleEffects {
  
 constructor(private actions$:Actions,private dbService:databaseService.DatabaseService){console.log("Ueffectu sam!")} 
   
 @Effect() //actions
 loadMovies$=this.actions$.ofType(peopleActions.GET_FROM_DATABASE)
  .pipe(
    switchMap(()=>{
    
        return this.dbService.getPeople()
       
        .pipe(
            map(people=>new peopleActions.DataArrived(people))
        )
    }))



}