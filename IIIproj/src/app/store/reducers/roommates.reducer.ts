import { Action } from "@ngrx/store"
import { People } from "../../models/people"
import {Roommate}from "../../models/roommates"
import {  GET_FROM_DATABASE_ROOMMATES, DATA_ARRIVED_ROOMMATES, DataArrivedRoommates,GetFromDatabaseRoommates,ADD_TO_DATABASE,AddToDataBase} from "../actions";
import {Room} from "../../models/room";
import {EntityState,createEntityAdapter,EntityAdapter} from '@ngrx/entity';
import {createFeatureSelector}from '@ngrx/store';


export const roommateAdapter:EntityAdapter<Roommate> =createEntityAdapter<Roommate>();

export interface State extends EntityState<Roommate>{};
const defaultRoommate={
    ids:['123'],
    entities:{
        '123':{
            id:'123',
            name:'Marko'
        }
    }
}
export const initialState:State=roommateAdapter.getInitialState();



export default function (state:State = initialState, action: Action) {
    switch (action.type) {
      
        case GET_FROM_DATABASE_ROOMMATES:{
            console.log("Dosao do ovde get from database roommate!");
           return roommateAdapter.removeAll(state);
        
           
            
        }
        case DATA_ARRIVED_ROOMMATES:{
            const roommatesList=(action as DataArrivedRoommates).roommates; 
           console.log(roommatesList);
          roommateAdapter.removeAll(state);
           return roommateAdapter.addAll(roommatesList, state);

            
        }  
        case ADD_TO_DATABASE:{
            console.log("U ROOMMATES!");
            const userName=(action as AddToDataBase).userName;
            const passWord=(action as AddToDataBase).passWord;
            const name=(action as AddToDataBase).name;
            const surName=(action as AddToDataBase).surName;
            const email=(action as AddToDataBase).email;
            const location=(action as AddToDataBase).location;
            const ages=(action as AddToDataBase).ages;
            const gender=(action as AddToDataBase).gender;
            const picture=(action as AddToDataBase).picture;
            console.log(userName,passWord);
         let room=new Array<Room>();
             const people=new People((Math.random() * 100) + 1,userName,passWord,name,surName,email,location,ages,picture,gender,room);
            console.log(state);
            return roommateAdapter.addOne(people,state);
        }  
        default: {
            return state
        }
    }
}

export const getRoommateState = createFeatureSelector<State>('roommate');
export const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
  } = roommateAdapter.getSelectors(getRoommateState);

