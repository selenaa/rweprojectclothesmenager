import { Action } from "@ngrx/store"
import { People } from "../../models/people"
import {Room}from "../../models/room"
import {  GET_FROM_DATABASE, DATA_ARRIVED,ADD_ROOM,DataArrived,GetFromDatabase,ADD_TO_DATABASE,AddToDataBase,AddRoom,DELETE_ROOM,DeleteRoom,EDIT_ROOM,EditRoom,UPDATE_VALUE,UpdateValue} from "../actions";

const initialState: People[] = [
 

]

export default function (state: People[] = initialState, action: Action) {
    switch (action.type) {
      
        case GET_FROM_DATABASE:{
            const newPeople=new People(0,(action as GetFromDatabase).userName,(action as GetFromDatabase).passWord,"","","","","","","",new Array<Room>());
          return [...state, 
            newPeople]
        } 
        case DATA_ARRIVED:{
            const peopleList=(action as DataArrived).people;
            peopleList.forEach(function(element)
        {
            if(element.username===state[0].username&&element.password===state[0].password)
            {
                return [
                    ...state,
                    state[0]=element,
                ]
            }
        })
            return [
                ...state
            ]
        }
        case ADD_TO_DATABASE:{
            
         let room=new Array<Room>();
           const newPeople=new People(state.length+1,(action as AddToDataBase).userName,(action as AddToDataBase).passWord,(action as AddToDataBase).name,(action as AddToDataBase).surName,(action as AddToDataBase).email,(action as AddToDataBase).location,(action as AddToDataBase).ages,(action as AddToDataBase).picture,(action as AddToDataBase).gender,new Array<Room>());
            console.log(state);
            return [
                ...state,
                newPeople,

            ]


        }
        case ADD_ROOM:{
            let  r=new Room("4",(action as AddRoom).lokacija,(action as AddRoom).brMesta,(action as AddRoom).slika,(action as AddRoom).description);
            state[0].room.push(r);
            console.log(state);
            return [
                ...state,]
        } 
        case DELETE_ROOM:{
          const room=(action as DeleteRoom).room;
     state[0].room.splice( state[0].room.indexOf(room),1);
      return [...state,]
    

        }
        case UPDATE_VALUE:{
            console.log("U reduceru sam za get from dataBase");
            let id=(action as UpdateValue).id;
           const element=state[0].room.find(el=>(el.id).toString()===id.toString())
           console.log(element);
               element.location=(action as UpdateValue).location;
              element.places=(action as UpdateValue).places;
              element.picture=(action as UpdateValue).picture;
              element.description=(action as UpdateValue).description;
            
            return [
                ...state,
            ]
        } 
       
        default: {
            return state
        }
    }
}