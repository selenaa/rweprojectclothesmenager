import { Action } from "@ngrx/store"
import { Room } from "../../models/room"
import { EDIT_ROOM, EditRoom } from "../actions";


 const initialState: Room[] = [
    
   
  ]
 export default function (state: Room[] = initialState ,action: Action) {
     switch (action.type) {
         case EDIT_ROOM:
             {
                 let room = (action as EditRoom).room
                 console.log(room);
                 state.push(room);

                 return [
                     ...state,
                 ]
                

             }
         default: {
             return state
         }
    }
 }


