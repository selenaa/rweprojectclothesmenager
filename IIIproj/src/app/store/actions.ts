import {Action} from '@ngrx/store'
import {People} from '../models/people'
import {Room}from '../models/room';
import {Roommate}from '../models/roommates';
export const GET_FROM_DATABASE='Get from database'
export const DATA_ARRIVED='Data arrived from db'
export const GET_FROM_DATABASE_ROOMS='Get from database rooms'
export const DATA_ARRIVED_ROOMS='Data arrived from db rooms'
export const GET_FROM_DATABASE_ROOMMATES='Get from database roommates'
export const DATA_ARRIVED_ROOMMATES='Data arrived from db roommates'
export const ADD_TO_DATABASE="Add to database"; 
export const ADD_ROOM="Add room";
export const DELETE_ROOM="Delete room";
export const EDIT_ROOM="Edite room";
export const UPDATE_VALUE="Update value"


export class GetFromDatabase implements Action{
    type=GET_FROM_DATABASE;

    constructor(public userName:string, public passWord:string){console.log("U get from database sam")}
}
export class DataArrived implements Action {
    type=DATA_ARRIVED;

    constructor(public people:People[]){}
}
export class AddToDataBase implements Action{
type=ADD_TO_DATABASE;
constructor(public userName:string, public passWord:string,public name:string,public surName:string,public location:string,public email:string,public ages:string,public gender:string,public picture:string){console.log("U get from database sam")}

}
export class DeleteRoom implements Action{
    type=DELETE_ROOM;

    constructor(public room:Room){};
}
export class EditRoom implements Action{
    type=EDIT_ROOM
    constructor(public room:Room){}
}
export class UpdateValue implements Action{
    type=UPDATE_VALUE;
    constructor(public id:string,public location:string,public places:string,public picture:string,public description:string){}

  
}
export class GetFromDatabaseRooms implements Action{
    type=GET_FROM_DATABASE_ROOMS;
    
    constructor(){console.log("U get from database sam za ROOMS!")}
}

export class DataArrivedRooms implements Action {
    type=DATA_ARRIVED_ROOMS;

    constructor(public rooms:Room[]){}
}

export class GetFromDatabaseRoommates implements Action{
    type=GET_FROM_DATABASE_ROOMMATES;
    
    constructor(){console.log("U get from database sam za ROOMMATES!")}
}

export class DataArrivedRoommates implements Action {
    type=DATA_ARRIVED_ROOMMATES;

    constructor(public roommates:Roommate[]){console.log("U DATA ARIVED ROOMATES SMO!")}
}
export class AddRoom implements Action{
    type=ADD_ROOM;
    constructor(public lokacija:string,public brMesta:string,public slika:string,public description:string){}
}