import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StoreModule } from "@ngrx/store";
import { rootReducer,effects } from './store';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { DatabaseService } from './services/database.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LogInComponent } from './components/log-in/log-in.component';
import { SingUpComponent } from './components/sing-up/sing-up.component';
import { ProfileComponent } from './components/profile/profile.component';
import {PeopleComponent} from './components/people/people.component';
import { RoomComponent } from './components/room/room.component';
import { RoomListComponent } from './components/room-list/room-list.component';
import { RoommateComponent } from './components/roommate/roommate.component';
import { RoommateListComponent } from './components/roommate-list/roommate-list.component';
import { AddRoomComponent } from './components/add-room/add-room.component';
import { RoomInProfileComponent } from './components/room-in-profile/room-in-profile.component';
import { EditRoomComponent } from './components/edit-room/edit-room.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser/platform-browser';
import{MaterialModule}from './material';

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    SingUpComponent,
    ProfileComponent,
    PeopleComponent,
    RoomComponent,
    RoomListComponent,
    RoommateComponent,
    RoommateListComponent,
    AddRoomComponent,
    RoomInProfileComponent,
    EditRoomComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot(rootReducer),
    EffectsModule.forRoot(effects),
  ],
  schemas:[
   CUSTOM_ELEMENTS_SCHEMA],
  exports:[
  
  ],
  providers: [DatabaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
