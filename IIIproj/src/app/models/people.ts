import{Room}from './room';
 
export class People {
    constructor(
        public id: number,
        public username: string,
        public password: string,
        public  name: string,
        public surname: string,
        public gender:string,
        public birthData:string,
        public email:string,
         public picture:string,
        public location:string,
        public room:Room[],
    ) {

    }
}