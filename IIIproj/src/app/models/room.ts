
export class Room {
    constructor(
        public id: string,
        public location: string,
        public places: string,
        public picture:string,
        public description: string
    ) {

    }
}