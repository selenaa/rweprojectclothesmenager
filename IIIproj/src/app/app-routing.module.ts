import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogInComponent }      from './components/log-in/log-in.component';
import{SingUpComponent} from './components/sing-up/sing-up.component';
import{ProfileComponent}from './components/profile/profile.component';
import{AddRoomComponent} from './components/add-room/add-room.component';
import {RoomListComponent}from './components/room-list/room-list.component';
import {RoommateListComponent}from './components/roommate-list/roommate-list.component';
import{EditRoomComponent}from'./components/edit-room/edit-room.component';


const routes: Routes = [
  { path: '', redirectTo: '/logIn', pathMatch: 'full' },
  { path: 'logIn', component: LogInComponent },
  {path:'edit',component:EditRoomComponent},
  { path: 'singUp', component: SingUpComponent },
  { path: 'add-room', component: AddRoomComponent },
  { path: 'room-list', component: RoomListComponent},
  { path: 'roommate-list', component: RoommateListComponent},
  { path: 'profile', component: ProfileComponent }, // treba da stoji profile/:id ovo bi trebalo da ukazuje da se otvori profil onog koji ima dati id tj koji se loguje u mom slucaju
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}