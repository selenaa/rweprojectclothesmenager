
import {OdevniPredmet} from './odevniPredmet';
import * as Rxjs from 'rxjs/Rx';
import'rxjs/add/operator/toPromise';
import {HtmlPageService} from "./html_page_service";
import {ImageEncodingService} from './slika_service';
import{DatabaseService}from './db_service';
import{NadjiSlaganje}from './nadji_slaganje'; 
import {VratiInner} from './boot_service';
import{VratiUspeh}from './boot_service';
import {Korisnik}from './korisnik';
import {Kombinacija}from './kombinacija';

//inicijalizacija
const html_page_service=new HtmlPageService();
const db_servis=new DatabaseService();
const slika_servis=new ImageEncodingService();
const trenutniKorisnik=new Korisnik(); 
const trazeniElement=new OdevniPredmet();
// 
html_page_service.initialize_login();

let divZaAkcije=document.createElement("div");
divZaAkcije.className="dza";
divZaAkcije.id="dza";
document.body.appendChild(divZaAkcije);
html_page_service.initialize_akcija(divZaAkcije);
divZaAkcije.style.display="none";

let divZaTrazenje=document.createElement("div");
divZaTrazenje.className="dzt";
divZaTrazenje.id="dzt";
document.body.appendChild(divZaTrazenje);
html_page_service.initialize(divZaTrazenje);
divZaTrazenje.style.display="none";

html_page_service.FormaZaRegistrovanje();
let divReg=document.getElementById("divZaReg");
divReg.style.display="none";

html_page_service.FormaZaDodavanje();
let divDod=document.getElementById("divZaDod");
divDod.style.display="none";


//funkcija za subscribe za godisnja doba
function zaSubscribeGD(ev){
    let i=1;
    let u=document.getElementById("email").value;
    let p=document.getElementById("pwd").value;
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===u&&element.password===p){
          for(i in element.clothes)
          {
              if(element.clothes[i].godisnjeDoba!=null){
            if(element.clothes[i].godisnjeDoba===ev)
            {
               html_page_service.PostaviSliku(element.clothes[i]);
            }
          }
        }
    }
        })
        let naslov=document.getElementById("naslov");
         naslov.innerHTML=`Vase stvari za: ${ev}`;
       setTimeout(()=>
       {
           html_page_service.nizDivova.forEach((el,index)=>{
               let naslov=document.getElementById("naslov");
               naslov.innerHTML="";  
               el.remove();
           })
   
       }, 5000);
     })
    clearTimeout();
}


  //kada menjam godisnja doba
  const leto_ob=Rxjs.Observable.fromEvent(document.getElementById("leto"),"change") 
  
  .subscribe((ev)=>{
     zaSubscribeGD(ev.target.value)
  });
const jesen_ob=Rxjs.Observable.fromEvent(document.getElementById("jesen"),"change") 
.subscribe((ev)=>{
    zaSubscribeGD(ev.target.value)
});

const zima_ob=Rxjs.Observable.fromEvent(document.getElementById("zima"),"change") 
.subscribe((ev)=>{
    zaSubscribeGD(ev.target.value)
});
const prolece_ob=Rxjs.Observable.fromEvent(document.getElementById("prolece"),"change") 
.subscribe((ev)=>{
    zaSubscribeGD(ev.target.value)
});

//predmet

const ob_tip=Rxjs.Observable.fromEvent(document.getElementById("odevniPredmet"),"change") 
.subscribe((ev)=>{
    let i=1;
    let u=document.getElementById("email").value;
    let p=document.getElementById("pwd").value;
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===u&&element.password===p){
          for(i in element.clothes)
          {
            if(element.clothes[i].tip===ev.target.value)
            {
               html_page_service.PostaviSliku(element.clothes[i]);
            }
          }
    }
        })
        let naslov=document.getElementById("naslov");
         naslov.innerHTML=`Vase/a/i ${ev.target.value}`;
       setTimeout(()=>
       {
           html_page_service.nizDivova.forEach((el,index)=>{
               let naslov=document.getElementById("naslov");
               naslov.innerHTML="";  
               el.remove();
           })
   
       }, 5000);
     })
    clearTimeout();
});

//prilika
const ob_prilika=Rxjs.Observable.fromEvent(document.getElementById("prilika"),"change") 

.subscribe((ev)=>{
    
    let i=1;
    let u=document.getElementById("email").value;
    let p=document.getElementById("pwd").value;
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===u&&element.password===p){
          for(i in element.clothes)
          {
            if(element.clothes[i].prilika===ev.target.value)
            {
               html_page_service.PostaviSliku(element.clothes[i]);
            }
          }
    }
        })
        let naslov=document.getElementById("naslov");
         naslov.innerHTML=`Stvari za priliku: ${ev.target.value}`;
       setTimeout(()=>
       {
           html_page_service.nizDivova.forEach((el,index)=>{
               let naslov=document.getElementById("naslov");
               naslov.innerHTML="";  
               el.remove();
           })
   
       }, 5000);
     })
    clearTimeout();
});

//boja
const ob_boja=Rxjs.Observable.fromEvent(document.getElementById("Boja"),"change") 
.subscribe((ev)=>{
    
    let i=1;
    let u=document.getElementById("email").value;
    let p=document.getElementById("pwd").value;
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===u&&element.password===p){
          for(i in element.clothes)
          {
            if(element.clothes[i].boja===ev.target.value)
            {
               html_page_service.PostaviSliku(element.clothes[i]);
            }
          }
    }
        })
        let naslov=document.getElementById("naslov");
         naslov.innerHTML=`Vase stvari boje:`;
       setTimeout(()=>
       {
           html_page_service.nizDivova.forEach((el,index)=>{
               let naslov=document.getElementById("naslov");
               naslov.innerHTML="";  
               el.remove();
           })
   
       }, 5000);
     })
    clearTimeout();
});    
//sarene stvari
const sara_ob_da=Rxjs.Observable.fromEvent(document.getElementById("da"),"change") 

.subscribe((ev)=>{
    console.log(ev.target.value);
    let i=1;
    let u=document.getElementById("email").value;
    let p=document.getElementById("pwd").value;
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===u&&element.password===p){
          for(i in element.clothes)
          {  
            if(element.clothes[i].sara==ev.target.value)
            {
               html_page_service.PostaviSliku(element.clothes[i]);
            }
          }
    }
        })
        let naslov=document.getElementById("naslov");
         naslov.innerHTML=`Vase sarene stvari:`;
       setTimeout(()=>
       {
           html_page_service.nizDivova.forEach((el,index)=>{
               let naslov=document.getElementById("naslov");
               naslov.innerHTML="";  
               el.remove();
           })
   
       }, 5000);
     })
    clearTimeout();

}); 

//stvari bez sare

const sara_ob_ne=Rxjs.Observable.fromEvent(document.getElementById("ne"),"change")
.subscribe((ev)=>{
    let i=1;
    let u=document.getElementById("email").value;
    let p=document.getElementById("pwd").value;
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===u&&element.password===p){
          for(i in element.clothes)
          {
            if(element.clothes[i].sara==ev.value)
            {
               html_page_service.PostaviSliku(element.clothes[i]);
            }
          }
    }
        })
        let naslov=document.getElementById("naslov");
         naslov.innerHTML=`Vase stvari bez sara:`;
       setTimeout(()=>
       {
           html_page_service.nizDivova.forEach((el,index)=>{
               let naslov=document.getElementById("naslov");
               naslov.innerHTML="";  
               el.remove();
           })
   
       }, 5000);
     })
    clearTimeout();

}); 

//za trazenje odevnog predmeta

const obs_btn=Rxjs.Observable.fromEvent(document.getElementById("dugme"),"click")
.subscribe((e)=>{
 
    let id="";
const gd=(document.querySelector('input[name="godisnjeDoba"]:checked')).value;
 console.log(gd);
 const Prilika= document.getElementById("prilika").options[document.getElementById("prilika").selectedIndex].value;
console.log(Prilika);
const odPredmet=document.getElementById("odevniPredmet").options[document.getElementById("odevniPredmet").selectedIndex].value;
console.log(odPredmet);
const Boja=document.getElementById("Boja").options[document.getElementById("Boja").selectedIndex].value;
console.log(Boja);
const Sara=(document.querySelector('input[name="sarica"]:checked')).value;
console.log(Sara);
let u=document.getElementById("email").value;
const nadji_slaganje=new NadjiSlaganje(u);
let p=document.getElementById("pwd").value;

fetch("http://localhost:3000/customers")
.then(response=>{
    if(response.status===404)
        return null;
    else
        return response.json();
})
.then(niz_objekata=>{
    niz_objekata
    .forEach((element,index)=>{
if(element.username===u&&element.password===p){
    let i=0;
  
      for(i in element.clothes)
      {  
        if(element.clothes[i].tip===odPredmet && element.clothes[i].prilika===Prilika &&element.clothes[i].godisnjeDoba===gd &&element.clothes[i].boja===Boja&&element.clothes[i].sara==Sara)
        {  
          trazeniElement.id=element.clothes[i].id;
          trazeniElement.tip=element.clothes[i].tip;
          trazeniElement.boja=element.clothes[i].boja;
          trazeniElement.sara=element.clothes[i].sara;
          trazeniElement.prilika=element.clothes[i].prilika;
         trazeniElement.godisnjeDoba= element.clothes[i].godisnjeDoba;
         trazeniElement.slika=element.clothes[i].slika
         trazeniElement.opis=element.clothes[i].opis;
         console.log(trazeniElement);
            html_page_service.LblPredlog();
           html_page_service.PostaviSliku(element.clothes[i]);
           id=element.clothes[i].id;
           console.log(id);
           
        }

      }  
     
      html_page_service.LblKombinacija();
      for(i in element.combinations)
              {  
            console.log(i);
            if(element.combinations[i].prvi.id!=id){     
            html_page_service.PostaviSliku(element.combinations[i].prvi);
            }
            else{
          if(element.combinations[i].drugi.id!=id){
             html_page_service.PostaviSliku(element.combinations[i].drugi);
                }
            }
              }
            }
        })
              if(id===null){
            let nijenadjen=document.createElement("div");
             nijenadjen.innerHTML=VratiInner("Ovakav odevni predmet ne postoji u bazi!");
             document.body.appendChild(nijenadjen);

              }
            })

             
.then(()=>{ 
    console.log("usao ovde");

    nadji_slaganje.po_godisnjemDobu(gd);
    nadji_slaganje.po_prilici(Prilika);
    nadji_slaganje.po_tipu(odPredmet);
    nadji_slaganje.po_boji(Boja);
    nadji_slaganje.po_sari(Sara);
  
    html_page_service.initialize_predlog(nadji_slaganje);

    nadji_slaganje.listaPredmeta.forEach((el)=>{

        let obs_predlog=Rxjs.Observable.fromEvent(document.getElementById(`predmet${el.id}`),"mousedown")
        .subscribe((e)=>{
            let div=document.getElementById(`predmet${el.id}`);
            let popup = document.getElementById("myPopup");
           popup.classList.toggle("show");
            popup.innerHTML=el.opis;
    
        }) 

        let obs_predlog2=Rxjs.Observable.fromEvent(document.getElementById(`predmet${el.id}`),"dblclick")
        .subscribe((e)=>
           {  let el2;
            if (confirm('Da li zelite da sacuvate ovu kombinaciju?')) {
                fetch("http://localhost:3000/customers")
                .then(response=>{
                    if(response.status===404)
                        return null;
                    else
                        return response.json();
                })
                .then(niz_objekata=>{ 

                    niz_objekata
                    .forEach((element,index)=>{
                if(element.username===u&&element.password===p){
                   
                        element.combinations.push(
                            {
                                "prvi": {
                                  "id": trazeniElement.id,
                                  "tip": trazeniElement.tip,
                                  "boja": trazeniElement.boja,
                                  "sara": trazeniElement.sara,
                                  "prilika": trazeniElement.prilika,
                                  "godisnjeDoba": trazeniElement.godisnjeDoba,
                                  "slika": trazeniElement.slika,
                                  "opis": trazeniElement.opis,
                                  "mode":"no-cors",
                                },
                                "drugi": {
                                  "id": el.id,
                                  "tip": el.tip,
                                  "boja": el.boja,
                                  "sara": el.sara,
                                  "prilika": el.prilika,
                                  "godisnjeDoba": el.godisnjeDoba,
                                  "slika": el.slika,
                                  "opis": el.opis,
                                  "mode": "no-cors"
                                }
                              });
                        el2=element;
                        console.log(el2);
                        db_servis.update_database("http://localhost:3000/customers",el2);
                       
                        
                    }});
                
            })

            } else {
                // Do nothing!
            }
            });
           })
    


}) })

//dodavanje predmeta

const ob_dodajp=Rxjs.Observable.fromEvent(document.getElementById("btndod"),"click") 

.subscribe((ev)=>{

    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{ 
         
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===trenutniKorisnik.username&&element.password===trenutniKorisnik.password){

        let i=0;
        let sara;
        for(i in element.clothes)
        { 
           i++;

         }
         if(document.getElementById("intip").value!="" && document.getElementById("inboja").value!="" && document.getElementById("insara").value!=""&& document.getElementById("inprilika").value!=""&& document.getElementById("ingd").value!="" &&  document.getElementById("inslika").value!=""&& document.getElementById("inopis").value){
         if(document.getElementById("insara").value==="Da"){

          sara=1;
         }
         else
         {
             sara=0;
         }
            element.clothes.push(
                {
                      "id":i+1, 
                      "tip": document.getElementById("intip").value,
                      "boja": document.getElementById("inboja").value,
                      "sara": sara,
                      "prilika": document.getElementById("inprilika").value,
                      "godisnjeDoba": document.getElementById("ingd").value,
                      "slika":  document.getElementById("inslika").value,
                      "opis":  document.getElementById("inopis").value,
                      "mode":"no-cors",
            
                  });
                  let el2=element;
            db_servis.update_database("http://localhost:3000/customers",el2);
           
                }
                else{

                    let al=document.createElement("div");
                    al.innerHTML=VratiInner("Neko polje nije uneto!Molimo unesite sva polja!");
                    document.body.appendChild(al);

                }

        }});
        
})


   
})

//dugme za dodavanje predmeta
const ob_dodajPredmet=Rxjs.Observable.fromEvent(document.getElementById("dugmedodaj"),"click") 

.subscribe((ev)=>{

    if(document.getElementById("dzt").style.display==="block"){
        
               document.getElementById("dzt").style.display="none";
            }
     if( document.getElementById("divZaDod").style.display==="none")
             {
                document.getElementById("divZaDod").style.display="block";
             }
                fetch("http://localhost:3000/customers")
                .then(response=>{
                    if(response.status===404)
                        return null;
                    else
                        return response.json();
                })
                .then(niz_objekata=>{
                    niz_objekata
                    .forEach((element,index)=>{
                if(element.username===trenutniKorisnik.username&&element.password===trenutniKorisnik.password){
                    let i=0;
                      for(i in element.clothes)
                      {  
                          if(document.getElementById(`predmet${element.clothes[i].id}`)!=null)
                     { let el=document.getElementById(`predmet${element.clothes[i].id}`);
                       console.log(el);
                        el.remove();
                        document.getElementById("lblbrisanje").innerHTML="";
                
                        }
                    }
                    }
                })
            })                
   
    divDod.style.display="block";

})

//dugme za logovanje
const obs_nalog=Rxjs.Observable.fromEvent(document.getElementById("dugmeZaLog"),"click")
.subscribe((e)=>{
    let username=document.getElementById("email").value;
    trenutniKorisnik.username=username;
    let pass=document.getElementById("pwd").value;
    trenutniKorisnik.password=pass;
    console.log(username);
    console.log(pass);
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata.forEach((el)=>{
         if(el.username===username && el.password===pass)
         {
            let al=document.createElement("div");
            al.innerHTML=VratiUspeh("Uspesno ste se ulogovali na sajt!");
            document.body.appendChild(al);
           document.getElementById("divZaLog").style.display="none";
           document.getElementById("dza").style.display="block";
            html_page_service.kreirajNaslovZaPromene();
    
         } })  }) })

 //prijava ako nalog ne postoji

  const obs_btnPrijava=Rxjs.Observable.fromEvent(document.getElementById("prijava"),"click")
         .subscribe((e)=>{

       divReg.style.display="block";
              
         })

         //registracija naloga

        const obs_reg=Rxjs.Observable.fromEvent(document.getElementById("btnPrijava"),"click")
         .subscribe((e)=>{
            fetch("http://localhost:3000/customers")
            .then(response=>{
                if(response.status===404)
                    return null;
                else
                    return response.json();
            })
            .then(niz_objekata=>{
                let n=0;
            niz_objekata.forEach((e)=>{
             n++;

            })

            if(document.getElementById("imeIn").value===""|| document.getElementById("prezimeIn").value===""||document.getElementById("uNameIn").value===""||document.getElementById("passIn").value===""){
                let al1=document.createElement("div");
                al1.innerHTML=VratiInner("Niste uneli sva neophodna polja!Molimo pokusajte ponovo!");
                document.body.appendChild(al1);
            }
            else{
        trenutniKorisnik.id=n+1;
        trenutniKorisnik.username=document.getElementById("uNameIn").value;
        trenutniKorisnik.password=document.getElementById("passIn").value;
        trenutniKorisnik.ime=document.getElementById("imeIn").value;
        trenutniKorisnik.prezime=document.getElementById("prezimeIn").value;
        trenutniKorisnik.odeca="";
        trenutniKorisnik.kombinacije="";
        console.log(trenutniKorisnik);
        db_servis.add_to_database("http://localhost:3000/customers",trenutniKorisnik);
        let al=document.createElement("div");
        al.innerHTML=VratiUspeh("Uspesno ste kreirali vas nalog!Molimo Vas da se sada ulogujete!");
        document.body.appendChild(al);
        document.getElementById("divZaReg").style.visibility='hidden';
               }  
             })
         })
//korisnicko ime zauzeto

         const obs_User=Rxjs.Observable.fromEvent(document.getElementById("uNameIn"),"mouseout")
         .subscribe((e)=>{
             let userIN=document.getElementById("uNameIn").value;
            fetch("http://localhost:3000/customers")
            .then(response=>{
                if(response.status===404)
                    return null;
                else
                    return response.json();
            })
            .then(niz_objekata=>{
            let novi= niz_objekata.map(obj=>obj.username);
                console.log(novi);
                 var pom=novi.includes(userIN);
                 console.log(pom);
                  if(pom==true){
        
                 let al=document.createElement("div");
                 al.innerHTML=VratiInner("Ovaj username je vec zauzet!Molimo unesite drugo korisnicko ime!");
                 document.body.appendChild(al);
        
                 
                  }
          })
        })

 //za brisanje

        const obs_Brisi=Rxjs.Observable.fromEvent(document.getElementById("dugmebrisi"),"click")
        .subscribe((e)=>{

            if(document.getElementById("dzt").style.display==="block")
            document.getElementById("dzt").style.display="none";
            if(document.getElementById("divZaDod").style.display==="block")
            document.getElementById("divZaDod").style.display="none";

            let lblTrazi=document.createElement("h3");
            lblTrazi.className="brisanje";
            lblTrazi.id="lblbrisanje";
            lblTrazi.innerHTML="Dvostrukim klikom na predmet koji zelite da obrisete ga mozete obrisati:"
            document.body.appendChild(lblTrazi);

         
    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
        niz_objekata
        .forEach((element,index)=>{
    if(element.username===trenutniKorisnik.username&&element.password===trenutniKorisnik.password){
        let i=0;
          for(i in element.clothes)
          {  let rbr=i;
            html_page_service.PostaviSliku(element.clothes[i]);
            if(element.clothes[i]!=null){
            let obs_Brisanje=Rxjs.Observable.fromEvent(document.getElementById(`predmet${element.clothes[i].id}`),"dblclick")
            .subscribe((e)=>{
                
            
                let el2;
                if (confirm('Da li ste sigurni da zelite da izbrisete ovaj od odevni predmet?')) {
                    fetch("http://localhost:3000/customers")
                    .then(response=>{
                        if(response.status===404)
                            return null;
                        else
                            return response.json();
                    })
                    .then(niz_objekata=>{ 
    
                        niz_objekata
                        .forEach((element,index)=>{
                    if(element.username===trenutniKorisnik.username&&element.password===trenutniKorisnik.password){
                        console.log( element.clothes[rbr]);
                        let zaBrisanje=element.clothes[rbr];
                        let idzaBrisanje=element.clothes[rbr].id;
                        console.log(zaBrisanje);
                        console.log(idzaBrisanje);
                        
                        //novi niz odevnih predmeta tu prebacim sve sem ovog i onda njega prebacimm samo na kraj 
                       let j=0;
                       let nizPredmeta=[];
                       for(j in element.clothes){
                        nizPredmeta.push(element.clothes[j]);
                        j++

                       } 
                       console.log(nizPredmeta);
                       
                      let noviNiz=nizPredmeta.filter(oPredmet=>oPredmet.id!=idzaBrisanje);
                        element.clothes=noviNiz;
                       console.log(noviNiz);
                       html_page_service.ObrisiSliku(zaBrisanje);
                    
                           el2=element;

                         
                            console.log(el2);

                            db_servis.update_database("http://localhost:3000/customers",el2);
                           
                            
                        }});
                      
    
    
    
                    
                
                    
                })
    
                } else {
                    // Do nothing!
                }
            }) 
     
            i++;
           } }
        } 
        })

       


    }) })



//dugme za trazenje 
    const obs_btnPronadji=Rxjs.Observable.fromEvent(document.getElementById("dugmetrazi"),"click")
    .subscribe((e)=>{

        if(document.getElementById("divZaDod").style.display==="block"){
            
                   document.getElementById("divZaDod").style.display="none";
                }
         if( divZaTrazenje.style.display==="none")
                 {
                    divZaTrazenje.style.display="block";
                 }
                    fetch("http://localhost:3000/customers")
                    .then(response=>{
                        if(response.status===404)
                            return null;
                        else
                            return response.json();
                    })
                    .then(niz_objekata=>{
                        niz_objekata
                        .forEach((element,index)=>{
                    if(element.username===trenutniKorisnik.username&&element.password===trenutniKorisnik.password){
                        let i=0;
                          for(i in element.clothes)
                          {  
                              if(document.getElementById(`predmet${element.clothes[i].id}`)!=null)
                         { let el=document.getElementById(`predmet${element.clothes[i].id}`);
                           console.log(el);
                            el.remove();
                            document.getElementById("lblbrisanje").innerHTML="";
                    
                            }
                        }
                        }
                    })
                })                

                
        
    //divZaTrazenje.style.display="block";
    })