import datapicker from '../node_modules/js-datepicker/';
import TinyDatePicker from 'tiny-date-picker';
import {NadjiSlaganje}from './nadji_slaganje';
import{VratiInner}from'./boot_service';

export class HtmlPageService{

    constructor(){
        this.nizDivova=[];
    }
    initialize(divZaSVE){
        Promise.all([
            this.NaslovZaTrazenje(divZaSVE),
             this.initialize_leto(divZaSVE),
             this.initialize_jesen(divZaSVE),
             this.initialize_zima(divZaSVE),
             this.initialize_prolece(divZaSVE),
            this.initialize_prilika(divZaSVE),
            this. initialize_odevniPredmet(divZaSVE),  
            this. initialize_boja(divZaSVE),
            this.initialize_sara_da(divZaSVE),
            this.initialize_sara_ne(divZaSVE),
            this.initialize_btn(divZaSVE)
        ])
        }
 // izbor akcije

        initialize_akcija(divZaSve){
            Promise.all([
                this.initialize_pretrazivanje(divZaSve),
                this.initialize_dodajPredmet(divZaSve),
                this.initialize_obrisi(divZaSve),
           
            ])

        }



      //za leto
        
        initialize_leto(divZaSVE){
            return new Promise((resolve, reject) => {

                
               const tip=document.createElement("label");
               tip.innerHTML="Odaberite godisnje doba:";
               tip.className="leto";
              divZaSVE.appendChild(tip);

                const kontejnerLeto=document.createElement("div");
                kontejnerLeto.className="kontejnergd";
                divZaSVE.appendChild(kontejnerLeto);
   
   
               const leto=document.createElement("input");
               leto.type="radio";
               leto.value="leto";
               leto.id="leto";
               leto.name="godisnjeDoba";
               kontejnerLeto.appendChild(leto);
               const Leto=document.createElement("label");
               Leto.innerHTML="Leto";
               kontejnerLeto.appendChild(Leto);
               setTimeout(() => resolve(leto), 1000);
            });
         }
     //za jesen
         initialize_jesen(divZaSVE){

            return new Promise((resolve, reject) => {
            
                const kontejnergd=document.createElement("div");
                kontejnergd.className="kontejnergd";
                divZaSVE.appendChild(kontejnergd);


                const jesen=document.createElement("input");
                jesen.type="radio";
                jesen.value="jesen";
                jesen.id="jesen";
                jesen.name="godisnjeDoba";
                kontejnergd.appendChild(jesen);
                const Jesen=document.createElement("label");
                Jesen.innerHTML="Jesen";
                kontejnergd.appendChild(Jesen);
            
            setTimeout(() => resolve(jesen), 1000);
        });

         }

   //za zimu
         initialize_zima(divZaSVE){
            
         return new Promise((resolve, reject) => {
                        
          const kontejnergd=document.createElement("div");
        kontejnergd.className="kontejnergd";
        divZaSVE.appendChild(kontejnergd);
        const zima=document.createElement("input");
         zima.type="radio";
         zima.value="zima";
         zima.id="zima";
        zima.name="godisnjeDoba";
        kontejnergd.appendChild(zima);
        const Zima=document.createElement("label");
         Zima.innerHTML="Zima";
         kontejnergd.appendChild(Zima);
                        
        setTimeout(() => resolve(zima), 1000);
                    });
            
                     }

        //za prolece             
         initialize_prolece(divZaSVE){
                        
           return new Promise((resolve, reject) => {
                                    
          const kontejnergd=document.createElement("div");
          kontejnergd.className="kontejnergd";
         divZaSVE.appendChild(kontejnergd);
          const prolece=document.createElement("input");
         prolece.type="radio";
         prolece.value="prolece";
         prolece.id="prolece";
         prolece.name="godisnjeDoba";
          kontejnergd.appendChild(prolece);
          const Prolece=document.createElement("label");
         Prolece.innerHTML="Prolece";
          kontejnergd.appendChild(Prolece);
                                    
         setTimeout(() => resolve(prolece), 1000);
         });
                        
                                 }
        //za priliku        

        initialize_prilika(divZaSVE){
            return new Promise((resolve, reject) => {

                const kontejnerPrilika=document.createElement("div");
                kontejnerPrilika.className="kontejner";
                divZaSVE.appendChild(kontejnerPrilika);

            const prilika=document.createElement("label");
            prilika.innerHTML="Odaberite priliku:";
            kontejnerPrilika.appendChild(prilika);
            
            
            const zaPriliku=document.createElement("select");
            zaPriliku.id="prilika";
            kontejnerPrilika.appendChild(zaPriliku);

            const poslovna=document.createElement("option");
            poslovna.innerHTML="poslovna";
            poslovna.value="poslovna";
            zaPriliku.appendChild(poslovna);
            
            const svakodnevna=document.createElement("option");
            svakodnevna.innerHTML="svakodnevna";
            svakodnevna.value="svakodnevna";
            zaPriliku.appendChild(svakodnevna);
            
            const vecernja=document.createElement("option");
            vecernja.innerHTML=" vecernja";
            vecernja.value="vecernja";
            zaPriliku.appendChild(vecernja);
            
            const sportska=document.createElement("option");
            sportska.innerHTML=" sportska";
            sportska.value="sportska";
            zaPriliku.appendChild(sportska);
        
            setTimeout(() => resolve(zaPriliku), 1000);

          });
         }
         //za tip 
        initialize_odevniPredmet(divZaSVE){
            return new Promise((resolve, reject) => {

             const kontejnerPredmet=document.createElement("div");
                kontejnerPredmet.className="kontejner";
                divZaSVE.appendChild(kontejnerPredmet);

                const predmet=document.createElement("label");
                
                predmet.innerHTML="Danas nosim:";
                kontejnerPredmet.appendChild(predmet);
                
                
                const odevniPredmet=document.createElement("select");
                odevniPredmet.id="odevniPredmet";
               kontejnerPredmet.appendChild(odevniPredmet);
                
                const kosulja=document.createElement("option");
                kosulja.innerHTML="kosulja";
                odevniPredmet.appendChild(kosulja);

                const majica=document.createElement("option");
                majica.innerHTML="majica";
                odevniPredmet.appendChild(majica);

                const dzemper=document.createElement("option");
                dzemper.innerHTML="dzemper";
                odevniPredmet.appendChild(dzemper);
                
                const duks=document.createElement("option");
                duks.innerHTML="duks";
                odevniPredmet.appendChild(duks);

                const jakna=document.createElement("option");
                jakna.innerHTML="jakna";
                odevniPredmet.appendChild(jakna);

                const sako=document.createElement("option");
                sako.innerHTML="sako";
                odevniPredmet.appendChild(sako);
                
                const pantalone=document.createElement("option");
                pantalone.innerHTML="pantalone";
                odevniPredmet.appendChild(pantalone);

                const suknja=document.createElement("option");
                suknja.innerHTML="suknja";
                odevniPredmet.appendChild(suknja);

                const haljina=document.createElement("option");
                haljina.innerHTML="haljina";
                odevniPredmet.appendChild(haljina);

                const shorts=document.createElement("option");
                shorts.innerHTML="sorc";
                odevniPredmet.appendChild(shorts);

            setTimeout(() => resolve(odevniPredmet), 1000);
        });
         }

      //za boju
         initialize_boja(divZaSVE){
            return new Promise((resolve, reject) => {
               const kontejnerBoja=document.createElement("div");
               kontejnerBoja.className="kontejner";
               divZaSVE.appendChild(kontejnerBoja);


               let boja=document.createElement("label");
               boja.innerHTML="Odaberite boju:"
               
               kontejnerBoja.appendChild(boja);

              const Boja=document.createElement("select");
              Boja.id="Boja";
              kontejnerBoja.appendChild(Boja);

              
              const bela=document.createElement("option");
              bela.innerHTML="bela";
              bela.className="bela";
              Boja.appendChild(bela);
 
              const crna=document.createElement("option");
              crna.innerHTML="crna";
              Boja.appendChild(crna);

              const crvena=document.createElement("option");
              crvena.innerHTML="crvena";
              crvena.className="crvena";
              Boja.appendChild(crvena);

              const plava=document.createElement("option");
              plava.innerHTML="plava";
              Boja.appendChild(plava);

              const zuta=document.createElement("option");
              zuta.innerHTML="zuta";
              Boja.appendChild(zuta);

              const ljubicasta=document.createElement("option");
              ljubicasta.innerHTML="ljubicasta";
              Boja.appendChild(ljubicasta);

              const zelena=document.createElement("option");
              zelena.innerHTML="zelena";
              Boja.appendChild(zelena);

              const narandzasta=document.createElement("option");
              narandzasta.innerHTML="narandzasta";
              Boja.appendChild(narandzasta);

              const braon=document.createElement("option");
              braon.innerHTML="braon";
              Boja.appendChild(braon);


              const siva=document.createElement("option");
              siva.innerHTML="siva";
              Boja.appendChild(siva);

                setTimeout(() => resolve(Boja), 1000);
            });
             }

           //za sarenu odecu
             initialize_sara_da(divZaSVE){
                return new Promise((resolve, reject) => {

                    const kontejnerSara=document.createElement("div");
                  kontejnerSara.className="kontejnerSaraDa";
                    divZaSVE.appendChild(kontejnerSara);

                   const sara=document.createElement("label");
                   sara.innerHTML="Da li postoji sara";
                  kontejnerSara.appendChild(sara);

                   const da=document.createElement("input");
                   da.type="radio";
                   da.id="da";
                   da.name="sarica";
                   da.value=1;
                   kontejnerSara.appendChild(da);
                   const dalbl=document.createElement("label");
                   dalbl.innerHTML="Da";
                  kontejnerSara.appendChild(dalbl);

        
                    setTimeout(() => resolve(da), 1000);
                });
            }
            
        //za bez sara

            initialize_sara_ne(divZaSVE){
                return new Promise((resolve, reject) => {

                    const kontejnerSara=document.createElement("div");
                  kontejnerSara.className="kontejnerSaraNe";
                    divZaSVE.appendChild(kontejnerSara);
               const ne=document.createElement("input");
                   ne.type="radio";
                   ne.id="ne";
                   ne.name="sarica";
                   ne.value=0;
                   kontejnerSara.appendChild(ne);
                   const nelbl=document.createElement("label");
                   nelbl.innerHTML="Ne";
                  kontejnerSara.appendChild(nelbl);

                    setTimeout(() => resolve(ne), 1000);
                });
            }
           
        //za trazenje predmeta
    initialize_btn(divZaSVE){
        return new Promise((resolve, reject) => {
            let trazi=document.createElement("div");
            trazi.className="trazi";
            divZaSVE.appendChild(trazi);
            
            let btn=document.createElement("button");
            btn.id="dugme";
            btn.innerHTML="Trazi!";
            trazi.appendChild(btn);


            
            setTimeout(() => resolve(btn), 1000);
        });
    }

//za dodavanje predmeta
    initialize_dodajPredmet(divZaSVE){
        return new Promise((resolve, reject) => { 

    
          let dodaj=document.createElement("div");
          dodaj.className="kontejner";
          divZaSVE.appendChild(dodaj);

          let dodajlbl=document.createElement("h4");
          dodajlbl.innerHTML="Zelite  da dodate neki odevni predmet?"
          dodaj.appendChild(dodajlbl);
          
          let btndodaj=document.createElement("button");
          btndodaj.id="dugmedodaj";
          btndodaj.innerHTML="Dodaj predmet";
          dodaj.appendChild(btndodaj);

          let slicica=new Image();
          slicica.style.width=200;
          slicica.style.height=200;
         slicica.src="add2.jpg";
         divZaSVE.appendChild(slicica);
          
          setTimeout(() => resolve(btndodaj), 1000);
        
        })}

    //za brisanje
    initialize_obrisi(divZaSVE){
        return new Promise((resolve, reject) => {
            let brisi=document.createElement("div");
            brisi.className="kontejner";
            divZaSVE.appendChild(brisi);

            let brisilbl=document.createElement("h4");
            brisilbl.innerHTML="Zelite da obrisete neki odevni predmet?"
            brisi.appendChild(brisilbl);
            
            let btnbrisi=document.createElement("button");
            btnbrisi.id="dugmebrisi";
            btnbrisi.innerHTML="Brisi";
            brisi.appendChild(btnbrisi);
            let slicica=new Image();
            slicica.style.width=200;
            slicica.style.height=200;
           slicica.src="delete.jpg";
           divZaSVE.appendChild(slicica);
            
            setTimeout(() => resolve(btnbrisi), 1000);
        });
    }
//za pretrazivanje predmeta
    initialize_pretrazivanje(divZaSVE){
        return new Promise((resolve, reject) => {
            let traziPredmet=document.createElement("div");
            traziPredmet.className="kontejner";
            divZaSVE.appendChild(traziPredmet);

            let trazilbl=document.createElement("h4");
            trazilbl.innerHTML="Zelite da pronadjete odevnu kombinaciju za danas?"
            traziPredmet.appendChild(trazilbl);
            
            let btntrazi=document.createElement("button");
            btntrazi.id="dugmetrazi";
            btntrazi.innerHTML="Potrazi!";
            traziPredmet.appendChild(btntrazi);
            let slicica=new Image();
            slicica.style.width=200;
            slicica.style.height=200;
           slicica.src="find.jpg";
           divZaSVE.appendChild(slicica);
            
            setTimeout(() => resolve(btntrazi), 1000);
        });
    }




    //za nalazenje predloga 
    initialize_predlog(nadji_slaganje){

  return new Promise((resolve, reject) => {
   let predlozi=[];
    console.log("Predlozi");
   let predlog=document.createElement("h2");
  predlog.innerHTML="Vas predlog za kombinovanje";
  predlog.className="predlog";
 document.body.appendChild(predlog);
 nadji_slaganje.listaPredmeta.forEach((element)=>{
 const divZaPredmet=document.createElement("div");
 divZaPredmet.id=`predmet${element.id}`;
  console.log(divZaPredmet);
 divZaPredmet.className="predmet";
  predlozi.push(divZaPredmet);
  
  document.body.appendChild(divZaPredmet);
                        
                           //za popup
 const zaspan=document.createElement("span");
 zaspan.className="popuptext";
 zaspan.id="myPopup";
 divZaPredmet.appendChild(zaspan); 

let slicica=new Image();
slicica.style.width=200;
slicica.style.height=200;
 slicica.src=element.slika;
 divZaPredmet.appendChild(slicica);
setTimeout(() => resolve(predlozi), 1000);
                    });    

    });
}
//za logovanje

initialize_login(){
 
 return new Promise((resolve, reject) => {
  let niz=[];

   const divZaLog=document.createElement("div");
   divZaLog.id="divZaLog";
   document.body.appendChild(divZaLog);
   divZaLog.className="divZaLog"; 


   let naslovLog=document.createElement("h4");
   naslovLog.innerHTML="Logovanje na sajt:";
   naslovLog.className="divZaIme";
   divZaLog.appendChild(naslovLog);

   //divZaRegILog("divZaLog");

    let logIn=document.createElement("div");
    logIn.className="form-group";
 let userName=document.createElement("label");
 userName.for="email";
 userName.innerHTML="Email adress:";
 logIn.appendChild(userName);
  let userIn=document.createElement("input");
  userIn.type="email";
  userIn.className="form-control";
  userIn.id="email";
  logIn.appendChild(userIn);
  divZaLog.appendChild(logIn);
  userIn.onfocusout=function(){

    fetch("http://localhost:3000/customers")
    .then(response=>{
        if(response.status===404)
            return null;
        else
            return response.json();
    })
    .then(niz_objekata=>{
    let novi= niz_objekata.map(obj=>obj.username);
        console.log(novi);
        console.log(userIn.value);
         var pom=novi.includes(userIn.value);
         console.log(pom);
          if(pom==false){

         let al=document.createElement("div");
         al.innerHTML=VratiInner("Korisnicko ime ne postoji!Molimo pokusajte ponovo!");
         document.body.appendChild(al);

         
          }
  }) }
  
let logPass=document.createElement("div");
logPass.className="form-group";

 let pass=document.createElement("label");
 pass.for="pwd";
 pass.innerHTML="Password";
 logPass.appendChild(pass);
 let passIn=document.createElement("input");
 passIn.type="password"
 passIn.className="form-control";
 passIn.id="pwd";
 passIn.onfocusout=function(){
    
        fetch("http://localhost:3000/customers")
        .then(response=>{
            if(response.status===404)
                return null;
            else
                return response.json();
        })
        .then(niz_objekata=>{
        let novi= niz_objekata.filter(obj=>obj.username===userIn.value);
            console.log(novi);
            console.log(novi[0].password);
            console.log(passIn.value);
           if(novi[0].password=== passIn.value)
           {
        
           }
           else{
            let al=document.createElement("div");
            
                        al.innerHTML=VratiInner("Dati password ne odgovara korisnickom imenu koje ste uneli!Molimo probajte opet!");
                        document.body.appendChild(al);
           }
      }) }
    
 logPass.appendChild(passIn);
 divZaLog.appendChild(logPass);

 let btn=document.createElement("button");
 btn.id="dugmeZaLog";
 btn.innerHTML="Uloguj se!";
 divZaLog.appendChild(btn);
 niz.push(btn);

 let lblp=document.createElement("label");
 lblp.className="prijava";
 lblp.innerHTML="Nemate kreiran nalog?"
 divZaLog.appendChild(lblp);

 let btnPrijava=document.createElement("button");
 btnPrijava.id="prijava";
 btnPrijava.innerHTML="Registrujte se!";
 btnPrijava.className="prijavaBtn";
 divZaLog.appendChild(btnPrijava);
 niz.push(btnPrijava);

 setTimeout(() => resolve(niz), 1000);
   
    });
}

//za dodavanje novog odevnog predmeta
FormaZaDodavanje(){
   
    return new Promise((resolve, reject) => {
        

    const divZaDod=document.createElement("div");
    divZaDod.id="divZaDod";
    document.body.appendChild(divZaDod);
    divZaDod.className="divZaDod"; 

    let fzdNaslov=document.createElement("h3");
    fzdNaslov.innerHTML="Dodaj predmet:"
    divZaDod.appendChild(fzdNaslov);


      let lblgd=document.createElement("labela");
          lblgd.className="labelaDodaj";
          lblgd.innerHTML="Unesite godinje doba:prolece/leto/jesen/zima";
          divZaDod.appendChild(lblgd);     
          
          let ingd=document.createElement("input");
          ingd.className="inputDodaj";
          ingd.id="ingd";
          divZaDod.appendChild(ingd);

          let lblprilika=document.createElement("labela");
          lblprilika.className="labelaDodaj";
          lblprilika.innerHTML="Unesite  priliku:svakodnevna/poslovna/vecernja/sportska";
          divZaDod.appendChild(lblprilika);     
          
          let inprilika=document.createElement("input");
          inprilika.className="inputDodaj";
          inprilika.id="inprilika";
          divZaDod.appendChild(inprilika);

          let lbltip=document.createElement("labela");
          lbltip.className="labelaDodaj";
          lbltip.innerHTML="Unesite odevni predmet:kosulja/majica/dzemper/duks/sako/jakna/pantalone/suknja/haljina/sorc";
          divZaDod.appendChild(lbltip);     
          
          let intip=document.createElement("input");
          intip.className="inputDodaj";
          intip.id="intip";
          divZaDod.appendChild(intip);


          let lblboja=document.createElement("labela");
          lblboja.className="labelaDodaj";
          lblboja.innerHTML="Unesite boju:bela/crna/crvena/plava/zuta/ljubicasta/zelena/narandzasta/braon/siva";
          divZaDod.appendChild(lblboja);     
          
          let inboja=document.createElement("input");
          inboja.className="inputDodaj";
          inboja.id="inboja";
          divZaDod.appendChild(inboja);



          let lblsara=document.createElement("labela");
          lblsara.className="labelaDodaj";
          lblsara.innerHTML="Unesite postojanje sare:Da/Ne";
          divZaDod.appendChild(lblsara);     
          
          let insara=document.createElement("input");
          insara.className="inputDodaj";
          insara.id="insara";
          divZaDod.appendChild(insara);


          let lblslika=document.createElement("labela");
          lblslika.className="labelaDodaj";
          lblslika.innerHTML="U ovo polje unesite link do slike predmeta koji dodajeta";
          divZaDod.appendChild(lblslika);     
          
          let inslika=document.createElement("input");
          inslika.className="inputDodaj";
          inslika.id="inslika";
          divZaDod.appendChild(inslika);


          let lblopis=document.createElement("labela");
          lblopis.className="labelaDodaj";
          lblopis.innerHTML="Unesite kratak opis odevnog predmeta koji dodajete";
          divZaDod.appendChild(lblopis);     
          
          let inopis=document.createElement("input");
          inopis.className="inputDodaj";
          inopis.id="inopis";
          divZaDod.appendChild(inopis);


          let btndod=document.createElement("button");
          btndod.className="btnod";
          btndod.id="btndod";
          btndod.innerHTML="Dodaj predmet";
          divZaDod.appendChild(btndod);
          setTimeout(() => resolve(btndod), 1000)
          
          })


}



//ako nema kreiran nalog mora da ga kreira //
FormaZaRegistrovanje(){

    return new Promise((resolve, reject) => {

     let nizReg=[];
        const divZaReg=document.createElement("div");
        divZaReg.id="divZaReg";
        document.body.appendChild(divZaReg);
        divZaReg.className="divZaReg"; 

        let naslovReg=document.createElement("h4");
        naslovReg.innerHTML="Registracija";
        naslovReg.className="divZaIme";
        divZaReg.appendChild(naslovReg);
    
         let divZaIme=document.createElement("div");
         divZaIme.className="divZaIme";
         divZaReg.appendChild(divZaIme);

         let ime=document.createElement("label");
         ime.innerHTML="Unesite  vase ime";
         ime.className="ime";
         divZaIme.appendChild(ime);

         let imein=document.createElement("input");
         imein.type="text";
         imein.className="inputime";
         imein.id="imeIn";
         divZaIme.appendChild(imein);
         nizReg.push(imein);
         
         let divZaPrezime=document.createElement("div");
         divZaPrezime.className="divZaIme";
         divZaReg.appendChild(divZaPrezime);

         let prezime=document.createElement("label");
         prezime.innerHTML="Unesite  vase prezime";
         prezime.className="ime";
         divZaPrezime.appendChild(prezime);

         let prezimein=document.createElement("input");
         prezimein.type="text";
         prezimein.className="inputime";
         prezimein.id="prezimeIn";
         divZaPrezime.appendChild(prezimein);
         nizReg.push(prezimein);
          
         let divaZaUsername=document.createElement("div");
         divaZaUsername.className="divZaIme";
         divZaReg.appendChild(divaZaUsername);

         let uName=document.createElement("label");
         uName.innerHTML="Unesite username";
         uName.className="ime";
         divaZaUsername.appendChild(uName);


         let uNameIn=document.createElement("input");
         uNameIn.type="text";
         uNameIn.id="uNameIn";
         uNameIn.className="inputime";
         divaZaUsername.appendChild(uNameIn);
         nizReg.push(uNameIn);
        
         let divaZaPass=document.createElement("div");
         divaZaPass.className="divZaIme";
         divZaReg.appendChild(divaZaPass);

         let pass=document.createElement("label");
         pass.innerHTML="Unesite password";
         pass.className="ime";
         divaZaPass.appendChild(pass);

         let passIn=document.createElement("input");
         passIn.type="text";
         passIn.id="passIn";
         passIn.className="inputime";
         divaZaPass.appendChild(passIn);
         nizReg.push(passIn);

         let btnPrijava=document.createElement("button");
         btnPrijava.innerHTML="Registruj svoj nalog";
         btnPrijava.id="btnPrijava";
         divZaReg.appendChild(btnPrijava);
         nizReg.push(btnPrijava);
       // console.log(nizReg);
      setTimeout(() => resolve(nizReg), 1000)

})}
//naslovZaPromenu
kreirajNaslovZaPromene(){
    let labela=document.createElement("h4");
    labela.className="naslov";
    labela.id="naslov";
    document.body.appendChild(labela);
}
NaslovZaTrazenje(divZaSVE){
let pretraga=document.createElement("h4");
pretraga.className="pretraga";
pretraga.innerHTML="Trazenje odevne kombinacije"
divZaSVE.appendChild(pretraga);

let slicica=new Image();
slicica.className="slicica";
slicica.src="trazenje.png";
divZaSVE.appendChild(slicica);
let lblpretraga=document.createElement("h6");
lblpretraga.innerHTML="Odevni predmet koji zelite da nosite:"
divZaSVE.appendChild(lblpretraga);


}

PostaviSliku(el){
   if(el!=null){
    const divZaPredmet=document.createElement("div");
    divZaPredmet.id=`predmet${el.id}`;
    divZaPredmet.className="predmet";
    this.nizDivova.push(divZaPredmet);
    document.body.appendChild(divZaPredmet);
   let slicica=new Image();
    slicica.style.width=200;
    slicica.style.height=200;

   slicica.src=el.slika;
   divZaPredmet.appendChild(slicica); }
}
ObrisiSliku(el){
 let divSl=document.getElementById(`predmet${el.id}`);
 divSl.remove();

}
LblPredlog(){
    let predlog=document.createElement("h3");
    predlog.innerHTML="Trazeni komad";
    predlog.className="predlog";
    document.body.appendChild(predlog);
}
LblKombinacija(){
let predlog=document.createElement("h3");
predlog.innerHTML="Predhodno kombinovano sa:";
predlog.className="predlog";
document.body.appendChild(predlog);
}


}

