export class OdevniPredmet{
    constructor(id,tip,boja,sara,prilika,godisnjeDoba,slika,opis){
        this.id=id;
        this.tip=tip;
        this.boja=boja;
        this.sara=sara;
        this.godisnjeDoba=godisnjeDoba;
        this.prilika=prilika;
        this.slika=slika;
        this.opis=opis;
        this.mode="no-cors";
    }
    prikazi(){
        console.log(this.tip,this.sara,this.prilika,this.godisnjeDoba,this.slika,this.opis);
    }
}