export function VratiInner(naslov){
    
        const htmlstring=`
      
      
        <div class="alert alert-warning alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Upozorenje!</strong> ${naslov}
      </div>      
        `;

        return htmlstring;
    }
    export function VratiUspeh(natpis){
        const htmlstring=`
        
        
          <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Uspeh!</strong> ${natpis}
        </div>      
          `;
          return htmlstring;

    }
    export function SacuvajKombinaciju(){

      const htmlstring=`
      
      
        <div class="alert alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Upozorenje!</strong> Da li zelite da sacuvate ovu kombinaciju?<a href=http://www.nextdirect.com/rs/en></a> 
      </div>      
        `;

        return htmlstring;

    }

    