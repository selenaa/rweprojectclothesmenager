export class ImageEncodingService{
    
        base64_from_URL(url){
    
            {
                let img = new Image();
            
                img.setAttribute('crossOrigin', 'anonymous');
            
                img.onload = function () {
                    let canvas = document.createElement("canvas");
                    canvas.width =this.width;
                    canvas.height =this.height;
            
                    let ctx = canvas.getContext("2d");
                    ctx.drawImage(this, 0, 0);
            
                    let dataURL = canvas.toDataURL("image/png");
            
                    alert(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
                };
            
                img.src = url;
            }
    
        }//zovem sa base64_from_URL("https://urlslike.png") i dobijem base 64 string koji mogu da cuvam u bazi
        
        image_from_base64(base64_from_base){
            let image = new Image();
            image.src = 'data:image/jpg;base64,'+base64_from_base;
            return image;
    
        }//vratice mi sliku koju cu moci da koristim posle
        toDataURL(url)
        {
        return fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
          const reader = new FileReader()
          reader.onloadend = () => resolve(reader.result)
          reader.onerror = reject
          reader.readAsDataURL(blob)
        }))
        }
    } 