//import { QUOTE_ADD } from "../actions";
import {FLAG_TOP10}from "../actions";
import {NADJI_AUTORA}from "../actions";
import{NADJI_QUOTE}from"../actions";
import{DODAJ_QUOTE}from"../actions";
import {ALL_QUOTES}from "../actions";
import {ADD_LIKE}from "../actions";
import {RECEIVE_API_DATA} from "../actions/index";


 const initialState= {

        flegovi:[
        {top10:false},
        {autor:false},
        {find:false},
        {dodaj:false},
        {svi:false}

     ],
 } ;
export default function(state = initialState, action){
    switch(action.type){
        case FLAG_TOP10:{
     return {
    ...state,
    flegovi:[
       {top10: true},
       {autor:false},
       {find:false},
       {dodaj:false},
        {svi:false},
    ] 
     }
        }
       case NADJI_AUTORA:{
        return   {
            ...state,
            flegovi:[
               {top10: false},
               {autor:true},
               {find:false},
               {dodaj:false},
                {svi:false},
            ] 
             }

    }
    case NADJI_QUOTE:{
       return {
        ...state,
        flegovi:[
           {top10: false},
           {autor:false},
           {find:true},
           {dodaj:false},
            {svi:false},
        ] 
         }

    }
    case DODAJ_QUOTE:{
       return {
        ...state,
        flegovi:[
           {top10: false},
           {autor:false},
           {find:false},
           {dodaj:true},
            {svi:false},
        ] 
         }
       

    }
    case ALL_QUOTES:{
   return {
    ...state,
    flegovi:[
       {top10: false},
       {autor:false},
       {find:false},
       {dodaj:false},
        {svi:true},
    ] 
     };
    }
            return state;
         
            
  
     }

    return state; 

}