export const QUOTE_ADD = "QUOTE_ADD";
export const REQUEST_API_DATA="REQUEST_API_DATA";
export const RECEIVE_API_DATA="RECEIVE_API_DATA";
export const FLAG_TOP10="FLAG_TOP10";
export const NADJI_AUTORA="NADJI_AUTORA";
export const NADJI_QUOTE="NADJI_QUOTE";
export const LIKE_API_DATA="LIKE_API_DATA";
export const LIKE_API_DATA2="LIKE_API_DATA2";
export const ADD_API_DATA="ADD_API_DATA";
export const QUOTE_FIND_AUTHOR="QUOTE_FIND_AUTHOR";
export const QUOTE_FIND_TWO="QUOTE_FIND_TWO";
export const SEARCH_API_DATA="SEARCH_API_DATA";
export const SEARCH_API_DATA2="SEARCH_API_DATA2";
export const DODAJ_QUOTE="DODAJ_QUOTE";
export const ALL_QUOTES="ALL_QUOTES";
export const ADD_LIKE="ADD_LIKE";
export const ADD_DISLIKE="ADD_DISLIKE";

export const likeApiData= id=>(console.log("usao u like Api data!"), {type:LIKE_API_DATA,payload:id}) //request api dat
export const requestApiData= () =>({type:REQUEST_API_DATA});
export const receiveApiData=data => ({type:RECEIVE_API_DATA,payload:data});
export const addApiData=state=>({type:ADD_API_DATA,payload:state});
export const searchData=(nekiString)=>({type:SEARCH_API_DATA,payload:nekiString});
export const searchData2=(nekiString2)=>({type:SEARCH_API_DATA2,payload:nekiString2});
export function addQuote(quote){
       return { 
            type: QUOTE_ADD,
            payload: quote,
        }   
    }

export function flagTop10(){
    console.log("Usao u akciju");
    
    return{
        type:FLAG_TOP10,
        payload:1,

     

    }

}
export function nadjiAutora()
{
    return{
        type:NADJI_AUTORA,
        payload:2,

     

    }

}
export function nadjiQuote(){
    return{
        type:NADJI_QUOTE,
        payload:3,
    }
}
export function dodajQuote(){
   return{
       type:DODAJ_QUOTE,
       payload:4,
   }

}
export function allQuotes(){
    return{
        type:ALL_QUOTES,
        payload:5,
    }
}

export function addLike(quote){
        return{
            type:ADD_LIKE,
            payload:quote
        }
    }
    export function addDislike(quote){
        return{
            type:ADD_DISLIKE,
            payload:quote
        }

    }
    export function findQuote(unetAutor){
           return {
                type: QUOTE_FIND_AUTHOR,
                payload: unetAutor
            }
        }

     export function findQuote2(unetCitat){
               return {
                   type:QUOTE_FIND_TWO,
                   payload:unetCitat
                }
            
            }

