import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import {flagTop10}from './store/actions/index';
import{connect} from 'react-redux';
import{bindActionCreators} from 'redux';
import{nadjiAutora}from './store/actions/index';
import{nadjiQuote}from './store/actions/index';
import{dodajQuote}from './store/actions/index';
import{allQuotes}from './store/actions/index';

 class NavBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }
  render() {
    return (
      <div  className="nav">
        <Navbar  color="faded" light className="navbar">
          <NavbarBrand onClick={this.SviCitati} className="mr-auto"> all quotes</NavbarBrand>
          <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
          <Collapse isOpen={!this.state.collapsed} navbar>
            <Nav navbar>
              <NavItem>
                <NavLink onClick={this.Prvih10}>Top 10 quotes </NavLink>
              </NavItem>
              <NavItem>
                <NavLink onClick={this.Autor}>Find quote by author</NavLink>
              </NavItem>
              <NavItem>
                <NavLink  onClick={this.Find}>Find quote </NavLink>
              </NavItem>
              <NavItem>
                <NavLink onClick={this.Dodaj}>Add new quote </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
  SviCitati=()=>{
    this.props.all();
    console.log("SVI CITATI");
  
  }
Prvih10 =()=>{
  this.props.top10();
  console.log("PRVIH 10");

}
Autor=()=>{
  this.props.autor();
  console.log("AUTOR");

}
Find=()=>{
  this.props.find();
  console.log("FIND")

}
Dodaj=()=>{
  this.props.dodaj();
  console.log("DODAJ")

}
}



function mapStateToProps(state){

  return{
      flag:state.flag.flegovi[0].top10,
      flagAutor:state.flag.flegovi[1].autor,
      flagFind:state.flag.flegovi[2].find,
      flagDodaj:state.flag.flegovi[3].dodaj,
      flagAllQuotes:state.flag.flegovi[4].allQuotes,
  }
}

function MapDispatcherToProps(dispatch){
  return bindActionCreators({ 
    top10: flagTop10, 
    autor:nadjiAutora,
    find:nadjiQuote,
    dodaj:dodajQuote,
    all:allQuotes

      
  }, dispatch);
}
export default connect(mapStateToProps , MapDispatcherToProps )(NavBar);
  

  
  

