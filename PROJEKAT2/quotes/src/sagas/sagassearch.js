import{call,put,takeEvery,takeLatest}from "redux-saga/effects";

import {SEARCH_API_DATA,receiveApiData}from "../store/actions";
import{ searchData }from "../api";

 function* SearchApiData(action){
    console.log("Usao u searchApiData");
    console.log(action);

try{

    const data=yield call(searchData,action.payload);
    console.log(data);
    yield put(receiveApiData(data));



}
catch(e){

    console.log(e);
}


}


export default function* mySagaSearch(){
   
    yield takeLatest(SEARCH_API_DATA,SearchApiData);
}