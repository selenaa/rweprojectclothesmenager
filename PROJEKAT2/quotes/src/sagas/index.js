
import {all, call, fork, put, takeLatest} from 'redux-saga/effects';
import  mySaga from './sagas';
import mySagaLike from'./sagasLike';
import mySagaAdd from './sagasAdd';
import mySagaSearch from './sagassearch';
import mySagaSearch2 from './sagassearch2';

export default function* root() {
    yield all([
        fork(mySaga),
        fork(mySagaLike),
        fork(mySagaAdd),
        fork(mySagaSearch),
        fork(mySagaSearch2)
     
    ])
}