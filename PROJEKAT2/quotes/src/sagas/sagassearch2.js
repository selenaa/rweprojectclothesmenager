import{call,put,takeEvery,takeLatest}from "redux-saga/effects";

import {SEARCH_API_DATA2,receiveApiData}from "../store/actions";
import{ searchData2 }from "../api";

 function* SearchApiData2(action){
    console.log("Usao u searchApiData2");
    console.log(action);

try{

    const data=yield call(searchData2,action.payload);
    console.log(data);
    yield put(receiveApiData(data));



}
catch(e){

    console.log(e);
}


}


export default function* mySagaSearch2(){
   
    yield takeLatest(SEARCH_API_DATA2,SearchApiData2);
}