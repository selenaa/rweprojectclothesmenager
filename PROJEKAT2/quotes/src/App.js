import React, { Component } from 'react';
import './App.css';
import Photos from './Carousel';
import NavBar from './Navbar';
import QuotesList from './components/quotes-list';
import QuoteAdd from './components/quotes-add';
import{Provider}from 'react-redux';
import Home from './components/Home';
import {Router, Route} from 'react-router';



    class App extends Component {
  render() {
    return (
      <div className="App">
     <h1>Find best quotes here!</h1>
    <NavBar></NavBar>
   
    <QuotesList></QuotesList>

      </div>
     
    );

  }

}



export default App;
