import React,{Component} from 'react';
import {connect} from 'react-redux';
import{bindActionCreators} from 'redux';
import {requestApiData} from "../store/actions/index";


class Home extends React.Component{
componentDidMount(){
  this.props.requestApiData();

}

quote=(x,i)=>(

    <div key={x.id.value}>
      <div>{x.quote}</div> 
      <div>{x.author}</div>  
       </div>
)

render(){
    const {quotes=[]}=this.props.data;
   
   return(
    <h1>
  {quotes.map(this.quote)}
        </h1>


   )
   


}



}


const mapStateToProps =state =>({data: state.data});


function MapDispatcherToProps(dispatch){
    return bindActionCreators({ 
       requestApiData
        
    }, dispatch);
}
export default connect(mapStateToProps , MapDispatcherToProps )(Home);
