import React , { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import{Button}from 'reactstrap';
import{addApiData}from "../store/actions";
import AllQuotes from "./allquotes";
import {addQuote} from "../store/actions"

class QuoteAdd extends Component{
    constructor(){
        super();
    }

render(){

    console.log(this.props.ceoNiz);
    return(
        
              <div>
                    <div className="divcic">
                        <br />
                        <h3>Enter details about quote you want to add:</h3>


                        <span>Enter the name of the author</span> 

                        <br/>
                        <input name="author"  onChange = {this.adaptirajImeAutora}></input>
                        
                        <br />
        
                        <span>Quote is about:</span>        

                        <br/>

                        <input name="about"  onChange = {this.adaptirajOCemuJe}></input>

                        <br/>

                        <span>Enter quote:</span>

                        <br/>

                        <input name="quote"  onChange = {this.adaptirajCitat}></input>
                        <br/>
        
                        <Button className="dugme" onClick = {() => this.props.dodajCitat(this.state)} >
                        Add quote!
                        </Button>

                      
                        
                    </div>
                 <AllQuotes></AllQuotes>
                </div>

                );
            
                }
           
        
                dodajCitat=(state) => {
                    this.props.addApiData(state);
                    console.log("NOVI!");
                    console.log(state);
                }

            
                 adaptirajImeAutora =  (event) => {
                    this.setState({
                        id:this.props.ceoNiz.length,
                        author: event.target.value
                    });
                    console.log("Adaptiraj state:",this.state);
                }
                adaptirajOCemuJe = (event) => {
                    this.setState({
                        about: event.target.value,
                       
                    })
                }

                adaptirajCitat=  (event) => {
                    this.setState({
                       quote: event.target.value,
                       likes:0,
                       dislikes:0,
                       comments:0
                    });
                    console.log("Adaptiraj state:",this.state);
                }
            }
            
            

            function mapStateToProps(state) {
                return {
                    ceoNiz:state.data.ceoNiz
                }
            }
            
            function MapDIspatcherToProps(dispatch) {
                return bindActionCreators({
                   dodajCitat: addQuote,
                    addApiData
                }, dispatch);
            }
            
            export default connect(mapStateToProps, MapDIspatcherToProps )(QuoteAdd);




