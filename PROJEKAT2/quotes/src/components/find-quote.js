import React , { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AllQuotes from "./allquotes";
import {searchData} from "../store/actions/index";
import {findQuote}from "../store/actions";
import QuotesFilterList from "./quotes-filterList";
class QuoteFinder extends Component{
    constructor(){
        super();
    }



    render(){
        return (
            <div>
            <div className="divFind">
                <label>Find by the author:</label>
                <input onChange={this.nadjiCitatOnChange}></input>
                
            </div>
            <QuotesFilterList></QuotesFilterList>
            </div>
        )
    }

    nadjiCitatOnChange = (event) => {
        let nekiString = event.target.value;
        this.props.nadjiCitat(nekiString);  
    }

}
function mapStateToProps(state){
    return{
        ceoNiz:state.data.ceoNiz
    }
}

function MapDispatcherToProps(dispatch){
    return bindActionCreators({ 
        searchData,
       nadjiCitat:findQuote
    }, dispatch);
}

export default connect(mapStateToProps, MapDispatcherToProps )(QuoteFinder);
