import React , { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import{ findQuote2}from "../store/actions";
import AllQuotes from "./allquotes"; 
import {searchData2} from "../store/actions/index";
import QuotesFilterList from "./quotes-filterList";


class QuoteFinder2 extends Component{
    constructor(){
        super();
    }
    render(){
        return (
            <div>
            <div className="divFind">
                <label>Find by the content</label>
                <input onChange={this.nadjiCitatOnChange}></input>
                
            </div>
            <QuotesFilterList></QuotesFilterList>
            </div>
        )
    }

    nadjiCitatOnChange = (event) => {
        let nekiString = event.target.value;
        this.props.nadjiCitat2(nekiString);
        
    }

}
function mapStateToProps(state){
    return{
       // ceoNiz:state.data.ceoNiz
    }
}

function MapDispatcherToProps(dispatch){
    return bindActionCreators({
      searchData2,
      nadjiCitat2 :findQuote2
    }, dispatch);
}

export default connect(mapStateToProps, MapDispatcherToProps )(QuoteFinder2);
