import React,{Component} from 'react';
import {connect} from 'react-redux';
import{bindActionCreators} from 'redux';
import {requestApiData} from "../store/actions/index";
import {likeApiData}from "../store/actions/index";
import {addDislike}from "../store/actions";
import {addLike} from "../store/actions";

class Top10 extends Component{
    componentDidMount(){
        
              this.props.requestApiData();
              
              
              
            }
    render()
{
    
    return (
        
                <ul>
                  
                {this.renderList()}    
            </ul>
                  );
   
}
renderList(){
    if(!this.props.top10)
    {
     return <li>Loading..</li>
    }
    return (this.props.top10.sort((a,b)=>(b.likes-b.dislikes)-(a.likes-a.dislikes)).map(quote => {
        return (
        
            <li key = {quote.id} > 
             <div className="divcic">
                <div className="text">
                {quote.quote}
            </div>
                <br/>
                -
                {quote.author}
               <br/>
               <div className="likes">
                        {quote.dislikes}
                        </div>
                        <div className="lajk"  onClick = {() => {
                            this.props.dodajUnlike(quote);
                       
                          
                        }}>
                         <img src="https://cdn1.iconfinder.com/data/icons/social-media-set-2/96/Unlike-512.png"  />
                    </div>   
                       <div className="likes">
                        {quote.likes}
                        </div>

                        <div className="lajk"  onClick = {() => {
                           this.props.dodajLike(quote);
                       
                        }}>
                         <img src="https://cdn1.iconfinder.com/data/icons/social-media-14/96/Like-512.png"  />
                    </div>
                        
                  
                        

                </div>
            
              

        
            </li>

        )
    }))
}


}


function mapStateToProps(state){
    console.log(state.data)
return{

   top10:state.data.top10
  
}
}

function MapDispatcherToProps(dispatch){
return bindActionCreators({ 
   requestApiData,
    dodajLike:addLike,
    dodajUnlike:addDislike
  

}, dispatch);
}
export default connect(mapStateToProps , MapDispatcherToProps )(Top10);


