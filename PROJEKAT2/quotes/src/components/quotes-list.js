import React,{Component} from 'react';
import {connect} from 'react-redux';
import{bindActionCreators} from 'redux';
import {requestApiData} from "../store/actions/index";
import Top10 from"./Top10";
import QuoteAdd from "./quotes-add";
import QuoteFinder from "./find-quote";
import QuoteFinder2 from "./find2-quote";
import Button from "reactstrap";
import Photos from '../Carousel';
import AllQuotes from './allquotes';
import {likeApiData}from "../store/actions/index";
import {addLike}from "../store/actions";


class QuotesList extends Component{
    componentDidMount(){

       this.props.requestApiData();
       console.log( this.props.requestApiData());
      // this.setState({ value: true });
      
      
    }
    

    render(){
     
        return (
            <ul>

                {this.renderList()}
            </ul>
        )
    
    }

   renderSaga(){

    let results=[];
    results=this.props.data;
    console.log("Rezultati",results);
    console.log("DATA",this.props.data);
    if(typeof results !== 'undefined' && results.length > 0)
    {  
        
        console.log("U IFU sam");
        console.log("results u ifu",results);
        return (
            results.map(quote=>{return (
                
                    <li key = {quote.id} > 
                      <div className="divcic">
                         <div className="text">
                         {quote.quote}
                     </div>
                         <br/>
                        -
                         {quote.author}
                         <br/>
                       <div className="likes">
                        {quote.likes}
                        </div>
                        <button className="btn btn-primary"  onClick = {() => {
                           this.dodajLike(quote);
                           
                        }
                            
                            
                            
                            } >Like</button>
                        
                         </div>
                    
                      
    
                
                     </li>
    
                 )
             })
        );
    }
    else {return (<div>Donji div</div>);}



   }
    renderList(){
      console.log("RENDER!")
       console.log(this.props.flag);
       console.log(this.props.flagAutor);
       console.log(this.props.flagFind);
       console.log(this.props.flagDodaj);
       console.log(this.props.filtriraniNiz);
      if (!this.props.flag){ //  {this.renderSaga()} izmedju <ul> i</ul>
            if(!this.props.flagAutor)
            if(!this.props.flagFind)
             
               if(!this.props.flagDodaj)
               if(!this.props.flagAllQuotes){
             return (
                <ul>
                <div className="photos">
                <Photos></Photos>
                </div>
                
                    </ul> 
            
            );}
            else{
                return(<li><AllQuotes></AllQuotes></li>)
            }
            else{
                return(<li><QuoteAdd></QuoteAdd></li>)
            }
            else{
                return(<li><QuoteFinder2></QuoteFinder2>
                 
                
                
                </li>)
             }
            else{
                return (

                   <li>
                    <QuoteFinder></QuoteFinder>
                   
                  </li>

                );
            }
        }
        else {
            return (<Top10></Top10>); 
          
        }                    
      

}
dodajLike=(quote)=>{
    this.props.likeApiData(quote.id);
 
}


}

function mapStateToProps(state){
   
    return{
       data:state.data, 
       
        flag:state.flag.flegovi[0].top10,
        flagAutor:state.flag.flegovi[1].autor,
        flagFind:state.flag.flegovi[2].find,
        flagDodaj:state.flag.flegovi[3].dodaj,
        flagAllQuotes:state.flag.flegovi[4].svi,
        filtriraniNiz:state.flag.filtriraniNiz
     
        

    }
}

function MapDispatcherToProps(dispatch){
    return bindActionCreators({ 
       requestApiData,
       dodajLike:addLike,
        likeApiData,
    
    }, dispatch);
}
export default connect(mapStateToProps , MapDispatcherToProps )(QuotesList);