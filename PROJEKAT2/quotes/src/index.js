import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from "./App";
import registerServiceWorker from './registerServiceWorker';
import rootReducer from './store/reducers/';
import {createStore} from 'redux'; 
import {applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import mySaga from './sagas/sagas';
import root from './sagas/index';
import {Provider}from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css'
import mySaga3 from './sagas/sagasLike2';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()
// mount it on the Store
 const store= createStore(
    rootReducer,
  applyMiddleware(sagaMiddleware)
)

// then run the saga
sagaMiddleware.run(root);
//store.runSaga=sagaMiddleware.run

ReactDOM.render(
    <Provider store={store}>
    <App/>


    </Provider> 
    , document.getElementById('root'));
registerServiceWorker();